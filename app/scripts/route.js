'use strict';

angular.module('topPlaylistClientApp').config(function($stateProvider, $urlRouterProvider, $translateProvider) {
  $stateProvider

    .state('home', {
      url: '/',
      templateUrl: 'views/home.html',
      controller: 'HomeCtrl',
      data: {
        ncyBreadcrumbLabel: "{{ 'route_states.home' | translate }}"
      }
    })

    .state('sources', {
      url: '/sources',
      templateUrl: 'views/sources.html',
      controller: 'SourcesCtrl',
      data: {
        ncyBreadcrumbLabel: "{{ 'route_states.sources' | translate }}"
      }
    })

    .state('sources.details', {
      url: '/{sourceId}',
      views: {
        "@" : {
          templateUrl: 'views/source/details.html',
          controller: 'SourceDetailsCtrl'
        }
      },
      data: {
        ncyBreadcrumbLabel: '{{ source.name }}'
      }
    })

    .state('sources.details.latest_ranks', {
      url: '/latest',
      views: {
        "@" : {
          templateUrl: 'views/source/ranks.html',
          controller: 'LatestRanksCtrl'
        }
      },
      data: {
        ncyBreadcrumbLabel: '{{ playlistReport.date }}'
      }
    })

    .state('sources.details.ranks', {
      url: '/{playlistReportId}/ranks',
      views: {
        "@" : {
          templateUrl: 'views/source/ranks.html',
          controller: 'RanksCtrl'
        }
      },
      data: {
        ncyBreadcrumbLabel: '{{ playlistReport.date }}'
      }
    })

    .state('songs_all', {
      url: '/songs',
      templateUrl: 'views/songs/index.html',
      controller: 'SongsCtrl',
      data: {
        ncyBreadcrumbLabel: "{{ 'route_states.songs_all' | translate }}"
      }
    })

    .state('songs_new', {
      url: '/songs/newest',
      templateUrl: 'views/songs/newest.html',
      controller: 'NewestSongsCtrl',
      data: {
        ncyBreadcrumbLabel: "{{ 'route_states.songs_new' | translate }}"
      }
    })

    .state('about', {
      url: '/about',
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl',
      data: {
        ncyBreadcrumbLabel: "{{ 'route_states.about' | translate }}"
      }
    })

    .state('song_rotations', {
      url: '/song-rotations',
      templateUrl: 'views/song_rotations.html',
      controller: 'SongRotationsCtrl',
      data: {
        ncyBreadcrumbLabel: "{{ 'route_states.song_rotations' | translate }}"
      }
    })

    .state('copyright_holders', {
      url: '/copyright-holders',
      templateUrl: 'views/copyright_holders.html',
      data: {
        ncyBreadcrumbLabel: "{{ 'route_states.copyright_holders' | translate }}"
      }
    });

  $urlRouterProvider.otherwise('/');

});