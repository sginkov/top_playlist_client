'use strict';

angular.module('topPlaylistClientApp').
  directive('scrollToTop', ['$document', function($document) {
	  var directive = { };
	  directive.restrict = 'AE';
	  directive.template = '<div id="toTop" ng-attr-title="{{ \'hint.back_to_top\' | translate }}" ng-click="scrollToTop()"  class="scroll-to-top"><i class="fa fa-arrow-up"></i></div>';
	  
	  directive.link = function(scope, elements, attr) {

	  	var scrollElement = $("#toTop");
			// Hide the toTop button when the page loads.
			scrollElement.css("display", "none");
		 
			// This function runs every time the user scrolls the page.
			$(window).scroll(function(){
		 
				// Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
				if($(window).scrollTop() > 1000){		 
					// If it's more than or equal to 0, show the toTop button.
					scrollElement.fadeIn("slow");
				}
		 		else {
		 		// If it's less than 0 (at the top), hide the toTop button.
			 		scrollElement.fadeOut("slow");
				}
		 	});

		 	scope.scrollToTop = function(){
				$document.scrollTopAnimated(0);
		 	}

	  };
	  
	  return directive;
  }
]);