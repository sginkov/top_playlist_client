'use strict';

angular.module('topPlaylistClientApp').
  directive('spinner', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'views/partials/_spinner.html'
    }
  }
);