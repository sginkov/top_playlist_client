angular.module('smart-table')
    .directive('stPaginationScroll', ['$timeout', 'Settings', function (timeout, Settings) {
        return{
            require: 'stTable',
            link: function (scope, element, attr, ctrl) {
                var itemByPage = Settings.perPage;
                var pagination = ctrl.tableState().pagination;
                var lengthThreshold = 200;
                var timeThreshold = 20;
                scope.nextPage = function () {
                    ctrl.slice(pagination.start + itemByPage, itemByPage);
                };
                var promise = null;
                var container = angular.element(document);
                var body = document.body;
                scope.tableState = ctrl.tableState();

                container.bind('scroll', function () {
                    var remaining = body.scrollHeight - (window.innerHeight + $(window).scrollTop());

                    //if we have reached the threshold and we scroll down
                    if (remaining < lengthThreshold) {

                        //if there is already a timer running which has no expired yet we have to cancel it and restart the timer
                        if (promise !== null) {
                            timeout.cancel(promise);
                        }
                        promise = timeout(function () {

                            if (!scope.stopPagination && !scope.isLoading){
                              scope.nextPage();
                            }

                            promise = null;
                        }, timeThreshold);
                    }
                });
            }

        };
    }]);
