'use strict';

angular.module('topPlaylistClientApp').directive('popover', function () {
    return {
        link: function(scope, element, attrs)
        {
            $(element)
              .popover();
        }
    }
})