'use strict';

angular.module('topPlaylistClientApp').directive('ngDraggable', function($document, $window, ApplicationStore){
  function makeDraggable(scope, element, attr) {
    var startX = 0;
    var startY = 0;

    var x = startX;
    var y = startY;

    if (attr.draggableName && ApplicationStore.get(attr.draggableName + 'X')){
    	x = ApplicationStore.get(attr.draggableName + 'X')
    }

    if (attr.draggableName && ApplicationStore.get(attr.draggableName + 'Y')){
    	y = ApplicationStore.get(attr.draggableName + 'Y')
    }

    element.css({
      top: y + 'px',
      left: x + 'px'
    });

    element.on('mousedown', function(event) {
      event.preventDefault();
      
      startX = event.pageX - x;
      startY = event.pageY - y;

      $document.on('mousemove', mousemove);
      $document.on('mouseup', mouseup);
    });

    function mousemove(event) {
      y = event.pageY - startY;
      x = event.pageX - startX;

      if (attr.draggableName){
      	ApplicationStore.set(attr.draggableName + 'X', x);
      	ApplicationStore.set(attr.draggableName + 'Y', y);
      }

      element.css({
        top: y + 'px',
        left: x + 'px'
      });
    }

    function mouseup() {
      $document.unbind('mousemove', mousemove);
      $document.unbind('mouseup', mouseup);
    }
  }
  return {
    link: makeDraggable
  };
});