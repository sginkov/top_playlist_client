'use strict';

angular.module('topPlaylistClientApp').
  directive('navMenu', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'views/partials/navmenu.html',
      link: function navMenuState(scope, element, attrs, controller){
      }
    }
  }
);