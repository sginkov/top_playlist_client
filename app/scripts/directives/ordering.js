'use strict';

angular.module('topPlaylistClientApp').
  directive('ordering', [function() {
	  var directive = { };
	  directive.restrict = 'AE';
	  directive.templateUrl = "views/partials/_ordering.html";
	  
	  directive.link = function(scope, elements, attr) {
		  scope.changeDirection = function(){
	      if (scope.orderDirection == 'asc') {
	        scope.orderDirection = 'desc'
	      }else{
	        scope.orderDirection = 'asc'
	      }
	    }

	    scope.changeOrder = function(orderField, orderDirection){
	      if (scope.orderField == orderField){
	        scope.changeDirection();
	      }else{
	        scope.orderDirection = orderDirection;
	      }
	      scope.orderField = orderField;
	      scope.reorder();
	    }

	  };
	  
	  return directive;
  }
]);