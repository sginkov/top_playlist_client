'use strict';

angular.module('topPlaylistClientApp').directive( 'itunesAudio', ['$rootScope', '$compile', '$sce', '$timeout', 'iTunesService', 'SongQuery', '$translate', 'Analytics', 'ApplicationStore', function ($rootScope, $compile, $sce, $timeout, iTunesService, SongQuery, $translate, Analytics, ApplicationStore) {
  return {
    templateUrl: 'views/partials/_itunes_player.html',
    controller: function ($scope, $element, $attrs) {
      var song;

      $scope.$on('change_volume', function(event, v){
        $scope.itunes.setVolume(v);
      });

      $scope.itunesProcessing = false;

      if ($attrs.ngSong){
        song = $scope.$eval($attrs.ngSong);
      }else{
        song = $scope.row;
      }

      $scope.$on('stop_playing', function(event, type, id){
        if (type != 'itunes' || id != song.id){
          $scope.itunes.pause();          
        }
      });

      $scope.itunesPlaylist = [];

      var songFullName = function(song){
        return song.artist + ' - ' + song.name;
      }

      $scope.playPause = function(){
        $scope.itunes.setVolume(ApplicationStore.get('player_volume') || 1);

        if (!$scope.itunes.playing){
          $rootScope.$broadcast('stop_playing', 'itunes', song.id);
        }

        if (!song.itunes)
        {
          $scope.itunesProcessing = true;
          var query = {
            term: SongQuery.prepareSearchTerm(song),
            limit: 1,
            entity: 'musicTrack'
          }
          iTunesService.get(query).$promise.then(function(response){
            if (response && response.results[0])
            {
              var itunes = response.results[0];
              song.itunes = itunes;
              $scope.itunes.load(
                { src: itunes.previewUrl, type: 'audio/mp3' }, true
              )
              $scope.itunes.play();
              Analytics.trackEvent('audio', 'itunes-play', songFullName(song));
            }else{
              var msg = $translate.instant('notification.failed.audio_file_not_found');
              toastr.error(msg);  
            }  
            $scope.itunesProcessing = false;
          });
        }else{
          $scope.itunes.playPause();
        }        
      }
    }
  };
}]);