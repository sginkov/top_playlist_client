'use strict';

angular.module('topPlaylistClientApp').
  directive('voiting', ['$rootScope', 'ResourceWrapper', 'Analytics', function($rootScope, ResourceWrapper, Analytics) {
	  var directive = { };
	  directive.restrict = 'AE';
	  directive.templateUrl = "views/partials/_voiting.html";

	  directive.scope = {
	  	voteable: '=',
	  	voteableType: '='
	  }

	  var ownVote = function(id, type){
      if ($rootScope.session && $rootScope.session.votes){
        return _.find($rootScope.session.votes, function(vote){
          return vote.voteable_id == id && vote.voteable_type == type;
        })
      }else{
        return false;
      }
    }

    var removeVote = function(id, type){
      if ($rootScope.session && $rootScope.session.votes){
        $rootScope.session.votes = _.reject($rootScope.session.votes, function(vote){
          return vote.voteable_id == id && vote.voteable_type == type;
        })
      }  	
    }

    var addVote = function(id, type){
      if ($rootScope.session && $rootScope.session.votes){
      	$rootScope.session.votes.push({ voteable_id: id, voteable_type: type })
      }   	
    }

  	var setVote = function(scope, status, errorCallback){
  		ResourceWrapper.query('UserInfo', 'set_vote', { 
  			query: { provider: $rootScope.session.provider, 
  				uid: $rootScope.session.mid, 
  				voteable_id: scope.voteable.id, 
  				voteable_type: scope.voteableType,
  				status: status 
  			},
  			errorCallback: errorCallback
  		});	  		
  	}

  	var like = function(scope){
  		scope.liked = true;
			scope.voteable.votes_count += 1;
			addVote(scope.voteable.id, scope.voteableType);
  	}

  	var dislike = function(scope){
			scope.liked = false;
			if (scope.voteable.votes_count != 0) {
		 		scope.voteable.votes_count -= 1;
		 		removeVote(scope.voteable.id, scope.voteableType);
			}
  	}


  	var setAnalytics = function(event, action, voteable){
  		Analytics.trackEvent(event, action, voteable.name + ' by ' + $rootScope.session.mid);
  	}
	  
	  directive.link = function(scope, elements, attr) {

	  	scope.$watch('voteable', function(s){
	  		scope.liked = ownVote(s.id, scope.voteableType);
	  	});

	    scope.like = function(){
	    	if ($rootScope.session){
		    	if (scope.liked) {
		    		dislike(scope);
		    		setAnalytics('vote-down', 'source', scope.voteable);
		    		setVote(scope, false, function(){
		    			like(scope);
		    		})
		    	}else{
		    		like(scope);
		    		setAnalytics('vote-up', 'source', scope.voteable);
		    		setVote(scope, true, function(){
			    		dislike(scope);
		    		});
		    	}
	    	}
	    }

	    scope.$on('get_votes', function(event){
	    	scope.liked = ownVote(scope.voteable.id, scope.voteableType);
	    })
	  };
	  
	  return directive;
  }
]);