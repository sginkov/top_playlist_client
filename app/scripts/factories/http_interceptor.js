'use strict';

angular.module('topPlaylistClientApp').factory('HttpInterceptorFactory', ['$q', '$rootScope',
  function($q, $rootScope){
    return {
      request: function(config) {
        return config || $q.when(config);
      },

      requestError: function(rejection) {
        return $q.reject(rejection);
      },

      response: function(response) {
        return response || $q.when(response);
      },

      responseError: function(rejection) {
        $rootScope.$broadcast('error_'+rejection.status, rejection.data || {});
        return $q.reject(rejection);
      }
    };
  }
]);