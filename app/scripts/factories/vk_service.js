'use strict';

angular.module('topPlaylistClientApp').factory('VkService', ['API', '$translate', function(API, $translate) {      
  var handleError = function(error){
    var messages = {
      "Captcha needed": captchaHandler
    }
    //messages[error.error_msg].call(error);
  }

  function callApi(method, data) {
    var dfr = $.Deferred();
    VK.Api.call(method, data, function(result) {
      if (result.error){
        dfr.reject(result.error);
      }else{
        dfr.resolve(result);
      }
    }, function(err) {
        dfr.reject(err);
    });
    return dfr.promise();
  }

  var vk = {
    data: {}, 
    appID: API.vkSettings.apiId,
    appPermissions: 8,

    init: function(){      
      VK.init({apiId: vk.appID});                      
    },

    //метод входа
    login:function(callback){                    
     
      function authInfo(response){     
        if(response.session){ 
          /* User is authorized successfully */ 
          vk.data.user = response.session.user;
          if (response.settings) {  
            /* Selected user access settings, if they were requested */  
            callback(vk.data.user); 
          }                          
        }else {
          toastr.error($translate.instant('error_status_messages.status_401')); 
        }     
      }                       

      VK.Auth.login(authInfo, vk.appPermissions);
    },

    getUserInfo: function(callback){
      VK.Auth.getLoginStatus(function(response) { 
        if(response.session){ 
          VK.Api.call('users.get', 
            { user_ids: response.session.mid, fields: "photo" }, 
            function(result){
              if (result && result.response && result.response[0]){
                callback(result.response[0]);
              }
            });
        }                   
      })       
    },

    //метод проверки доступа
    access: function(callback){
      VK.Auth.getLoginStatus(function(response) { 
        if(response.session){ 
          callback(vk.data.user);
        }else { 
          vk.login(callback);         
        }                    
      })     
    },

    logout: function(){
      VK.Auth.logout();
    },

    addAudio: function(data, callback){
      vk.access(function(user){
        var params = {
          audio_id: data.aid,
          owner_id: data.owner_id
        }

        callApi('audio.add', params).then(function(r){
          callback();
          toastr.success($translate.instant('notification.success.audio_file_added'));                      
        })
      });      
    },

    getAudio: function(data){
      vk.access(function(user){
        var params = {
          audio_ids: data.audio_ids
        }
        callApi('audio.get', params).then(function(r){
          var songs = r.response;
          data.callback(songs[0]);                      
        }, handleError)
      });      
    },

    searchAudio: function(data){
      vk.access(function(user){
        var params = {
          q: data.q,
          sort: data.sort || 2,
          count: data.count || 1,
          search_own: data.search_own || 1,
          auto_complete: data.auto_complete == null ? 0 : data.auto_complete
        }
        callApi('audio.search', params).then(function(r){
          var songs = r.response;
          data.callback(songs[1]);                      
        }, handleError)
      });
    }
  }

  vk.init();       
  return vk;     
  
}]);

