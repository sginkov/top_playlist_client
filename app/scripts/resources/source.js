'use strict';

angular.module('topPlaylistClientApp').factory( 'Source', [
  '$resource', 'API', function($resource, API){
    return $resource(
      API.baseURL + '/sources/:id/:action',
      { id: '@id' },
      {
        query:{
          isArray: false 
        },
        latest_report:{
          isArray: false,
          params:{
            action: 'latest_report'
          }
        },
        ranks:{
          isArray: false,
          params:{
            action: 'ranks'
          }
        }
      }
    );

  }
]);