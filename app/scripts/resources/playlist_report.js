'use strict';

angular.module('topPlaylistClientApp').factory( 'PlaylistReport', [
  '$resource', 'API', function($resource, API){
    return $resource(
      API.baseURL + '/playlist_reports/:id/:action',
      {  id: '@id' },
      {
        query:{
          isArray: false 
        },
        list:{
          method:'GET', 
          params: { 
            action: 'list'
          }
        }
      }
    );

  }
]);