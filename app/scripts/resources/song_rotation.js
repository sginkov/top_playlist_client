'use strict';

angular.module('topPlaylistClientApp').factory( 'SongRotation', [
  '$resource', 'API', function($resource, API){
    return $resource(
      API.baseURL + '/song_rotations/:id',
      { id: '@id' },
      {
        query:{
          isArray: false 
        }
      }
    );

  }
]);