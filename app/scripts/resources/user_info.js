'use strict';

angular.module('topPlaylistClientApp').factory( 'UserInfo', [
  '$resource', 'API', function($resource, API){
    return $resource(
      API.baseURL + '/user_info/:uid/:action',
      {  uid: '@uid' },
      {
        get_votes: {
          isArray: false,
          params: { 
            action: 'get_votes'
          }           
        },
        set_vote:{
          method: 'POST', 
          isArray: false, 
          params: { 
            action: 'set_vote'
          }
        }
      }
    );

  }
]);