'use strict';

angular.module('topPlaylistClientApp').factory( 'Song', [
  '$resource', 'API', function($resource, API){
    return $resource(
      API.baseURL + '/songs/:id/:action',
      {  id: '@id' },
      {
        query:{
          isArray: false 
        },
        list:{
          method:'GET', 
          isArray: false, 
          params: { 
            action: 'newest'
          }
        }
      }
    );

  }
]);