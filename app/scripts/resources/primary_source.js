'use strict';

angular.module('topPlaylistClientApp').factory( 'PrimarySource', [
  '$resource', 'API', function($resource, API){
    return $resource(
      API.baseURL + '/primary_sources',
      {},
      {
        query:{
          isArray: false 
        }
      }
    );

  }
]);