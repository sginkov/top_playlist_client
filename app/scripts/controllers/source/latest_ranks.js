'use strict';

angular.module('topPlaylistClientApp')
  .controller('LatestRanksCtrl', ['$scope', '$rootScope', '$stateParams', '$breadcrumb', 'ResourceWrapper', 'EndlessTableBuilder', 
  	function ($scope, $rootScope, $stateParams, $breadcrumb, ResourceWrapper, EndlessTableBuilder) {
      $scope.source = {};
  		$scope.playlistReport = {};

  		ResourceWrapper.query('Source', 'get', { 
  			query: { id: $stateParams.sourceId },
  			successCallback: function(response){
        	$scope.source = response; 
          $breadcrumb.getStatesChain()[2].ncyBreadcrumbLabel = response.name;
      	}
  		});

      ResourceWrapper.query('Source', 'latest_report', {
        query:{
          id: $stateParams.sourceId
        },
        successCallback: function(response){
          $scope.playlistReport = response; 
          $breadcrumb.getStatesChain()[3].ncyBreadcrumbLabel = response.date; 
        }
      });  

      $scope.search = function(){
        init();
      }

      $scope.reset = function(){
        $scope.q = null;
        init();
      }

      var init = function(){
        EndlessTableBuilder.build($scope, 'Source', { 
          query: { id: $stateParams.sourceId, sort_by: 'value', sort_order: 'asc', q: $scope.q },
          resourceMethod: 'ranks' 
        });

        $scope.decorateCollection = function(collection){
          return _.map(collection, function(r){ return angular.extend(r.song, { value: r.value });})
        }

        $scope.getPosition = function(position, song){
          return song.value;
        }

        $scope.getCollection();
      }

      init();
  }]);