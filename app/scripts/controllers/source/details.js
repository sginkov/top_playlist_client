'use strict';

angular.module('topPlaylistClientApp')
  .controller('SourceDetailsCtrl', ['$scope', '$rootScope', '$stateParams', '$breadcrumb', 'ResourceWrapper', 'TableBuilder', 
  	function ($scope, $rootScope, $stateParams, $breadcrumb, ResourceWrapper, TableBuilder) {
  		$scope.source = {};
  		ResourceWrapper.query('Source', 'get', { 
  			query: { id: $stateParams.sourceId },
  			successCallback: function(source){
        	$scope.source = source; 
          $breadcrumb.getStatesChain()[2].ncyBreadcrumbLabel = source.name;   
      	}
  		});

  		TableBuilder.build($scope, 
  			'PlaylistReport', 
  			{ query: { source_id: $stateParams.sourceId } }
  		);
  }]);