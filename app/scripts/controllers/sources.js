'use strict';

angular.module('topPlaylistClientApp')
  .controller('SourcesCtrl', ['$scope', 'TableBuilder', function ($scope, TableBuilder) {
  	TableBuilder.build($scope, 'Source');
  }]);