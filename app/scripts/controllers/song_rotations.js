'use strict';

angular.module('topPlaylistClientApp')
  .controller('SongRotationsCtrl', ['$scope', 'EndlessTableBuilder', 'ResourceWrapper', 'ApplicationStore', '$state', 'Analytics', 'Genres', '$translate',
    function ($scope, EndlessTableBuilder, ResourceWrapper, ApplicationStore, $state, Analytics, Genres, $translate) {

    var selected = function(collection, id){
      if (collection.length == 0){
        return true;
      }else{
        return !!_.find(collection, function(entity){ return entity.id == id; });
      }
    }

    $scope.selectedGenres = ApplicationStore.get('genres') || [];

    $scope.genres = _.map(Genres, 
      function(genre){ 
        return { id: genre, 
          title: $translate.instant('genres.'+genre),
          selected: selected($scope.selectedGenres, genre), 
        }; 
      });

  	$scope.sources = ApplicationStore.get('sources') || [];
    $scope.allSources = $scope.sources;
    $scope.selectedSources = $scope.sources;

    var filterSource = function(genres, sources){
      if (genres){
        return _.filter(sources, function(s){
          return _.find(genres, function(g){ 
            var result = (g.id == s.genre);
            if (result && selected($scope.sources, s.id)){
              s.selected = true;
            }
            return result;
          });
        });
      }else{
        return [];
      }
    }

    $scope.$watch('selectedGenres', function(genres){
      if (genres){
        $scope.selectedSources = filterSource(genres, $scope.allSources);
      }
    });

    var buildSourceQuery = function(){
      var source_ids = [];
      angular.forEach($scope.sources, function(source) {
        source_ids.push(source.id);
      });  
      var genres = [];
      angular.forEach($scope.selectedGenres, function(genre) {
        genres.push(genre.id);
      });   
      return { "source_ids[]": source_ids, 
        "genres[]": genres,
        sort_by: $scope.orderField, 
        sort_order: $scope.orderDirection,  
      };
    }
  	    
  	$scope.submitForm = function(){
      ApplicationStore.set('genres', $scope.selectedGenres);
      ApplicationStore.set('sources', $scope.sources);
      Analytics.trackEvent('chart', 'build', _.map($scope.sources, function(s){ return s.name; }).join(', '));
      $state.go($state.current, {}, {reload: true}); 
  	}


    $scope.orderField = 'weight';
    $scope.orderDirection = 'desc';

    $scope.orderFields = [
      {name: 'weight', direction: 'desc', title: 'song.ordering.weight' },
      {name: 'created_at', direction: 'desc', title: 'song.ordering.created_at' }
    ]

    var init = function(){
      $scope.stopPagination = false;

      EndlessTableBuilder.build($scope, 'SongRotation', { query: buildSourceQuery() });

      $scope.getCollection();
    }


    ResourceWrapper.query('Source', 'query', { 
      successCallback: function(response){
        var results = [];
        angular.forEach(response.entities, function(item){
          results.push({
            id:   item.id,
            name: item.name,
            genre: item.genre,
            selected: selected($scope.sources, item.id)
          });
        });
        $scope.allSources = results;
        $scope.selectedSources = filterSource($scope.selectedGenres, results);
      }
    }); 

    $scope.reorder = init;

    init();
  }]);