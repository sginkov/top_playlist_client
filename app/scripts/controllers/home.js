'use strict';

angular.module('topPlaylistClientApp')
  .controller('HomeCtrl', ['$scope', 'ResourceWrapper', 'Settings', 'ApplicationStore', 'API',
  function ($scope, ResourceWrapper, Settings, ApplicationStore, API) {
  	$scope.primarySources = ApplicationStore.get('primary_sources') || [];
    
    var generateRandomArray = function(count){
      var i, a = [];
      for (i = 0; i < count; i++) { 
        a.push(i);
      }
      return _.shuffle(a);
    }

    var settings = {
      successCallback: function(collection){
        $scope.primarySources = collection.entities;   
        ApplicationStore.set('primary_sources', $scope.primarySources);    
      }
    }

    ResourceWrapper.query('PrimarySource', 'query', settings);

    try { 
      if (API.vkSettings.groupId){
        VK.Widgets.Group("vk_group_posts", {mode: 0, width: "300", height: "400", color1: '1C1E26', color2: 'FC836B', color3: 'E1563A'}, API.vkSettings.groupId);  
      }
    }
    catch(error){
      console.error(error)
    }
  }]);
