'use strict';

angular.module('topPlaylistClientApp')
  .controller('SongsCtrl', ['$scope', 'EndlessTableBuilder', '$translate', function ($scope, EndlessTableBuilder, $translate) {
  	$scope.title = $translate.instant('route_states.songs_all');

    $scope.orderField = 'name';
    $scope.orderDirection = 'asc';

    $scope.orderFields = [
      {name: 'name', direction: 'asc', title: 'song.ordering.name' },
      {name: 'created_at', direction: 'desc', title: 'song.ordering.created_at' }
    ]

  	$scope.search = function(){
	    init();
  	}

  	$scope.reset = function(){
  		$scope.q = null;
  		init();
  	}

  	var init = function(){
      $scope.stopPagination = false;

	    EndlessTableBuilder.build($scope, 'Song', 
        { 
          query:{ 
            sort_by: $scope.orderField, 
            sort_order: $scope.orderDirection, 
            q: $scope.q 
          } 
        }
      );

	    $scope.getCollection();
  	}

    $scope.reorder = init;

  	init();
  }]);