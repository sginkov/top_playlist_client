'use strict';

angular.module('topPlaylistClientApp')
  .controller('NavmenuCtrl', ['$scope', '$rootScope', 'ApplicationStore', '$translate', 'VkService',
  	function ($scope, $rootScope, ApplicationStore, $translate, VkService) {
    	$scope.languages = ['ru', 'en']

    	$scope.login = VkService.login;
    	$scope.logout = VkService.logout;

    	var updateVkUser = function(){
	  		VkService.getUserInfo(function(userInfo){
	      	$scope.vkUser = userInfo;
	      	$scope.$apply()
	      });
    	}

    	updateVkUser();

      $scope.$on('vk_login', function(event){
        updateVkUser();
      })

      $scope.$on('vk_logout', function(event){
        $scope.vkUser = null;
        $scope.$apply()
      })

    	$scope.changeLanguage = function (langKey) {
        $rootScope.$emit('change_local', langKey);
  		};
  }]);
