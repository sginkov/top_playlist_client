'use strict';

angular.module('topPlaylistClientApp').filter('trustedSrc', ['$sce', function ($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url);
    };
}])