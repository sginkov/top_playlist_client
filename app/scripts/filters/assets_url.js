'use strict';

angular.module('topPlaylistClientApp').filter('assetsUrl', ['API', function (API) {
    return function(path) {
      var url;
      if (path){
        url =  API.assetsURL + path;
      }
      return url;
    };
}])