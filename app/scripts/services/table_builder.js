'use strict';

angular.module('topPlaylistClientApp').service('TableBuilder', ['ResourceWrapper', 'TableQuery', function(ResourceWrapper, TableQuery) {
  this.build = function(scope, resource, options){
    var tableOptions = angular.extend({}, TableQuery.defaultOptions(), options);
    scope.rowCollection = [];
    scope.isLoading = false;
    angular.extend(scope, tableOptions);
    scope.getCollection = function(tableState){
      scope.isLoading = true;
      scope.rowCollection = [];

      var tableQuery = TableQuery.prepareQueryOptions(tableState);

      angular.extend(tableQuery, tableOptions.query);

      var settings = {
        query: tableQuery,
        successCallback: function(collection){
          scope.rowCollection = collection.entities;
          scope.startIndex = tableState.pagination.start + 1;
          tableState.pagination.numberOfPages = Math.ceil(collection._metadata.total_count / tableState.pagination.number);
          scope.$emit('collection_loaded');
          scope.isLoading = false;         
        }
      }

      ResourceWrapper.query(resource, tableOptions.resourceMethod, settings);
    };
  }
}]);