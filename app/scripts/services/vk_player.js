'use strict';

angular.module('topPlaylistClientApp').service('VkPlayer', ['$rootScope', '$translate', 'SongQuery', 'VkService', 'ApplicationStore', 'Analytics', 'Settings', function($rootScope, $translate, SongQuery, VkService, ApplicationStore, Analytics, Settings) {
  var vkPlayerExtension = {
    currentSongIndex: 0, 
    currentSong: {},
    vkProcessing: false
  };

  var playSong = function(scope, song, position){
    if (!song.vk)
    {
      vkSearch(scope, song, position, function(vk){
        scope.addSongToPlaylist(
          { src: vk.url, type: 'audio/mpeg' }
        )
        scope.mediaPlayer.play();
      });
    }else{
      scope.addSongToPlaylist(
        { src: song.vk.url, type: 'audio/mpeg' }
      )
      scope.mediaPlayer.play();
    }
    Analytics.trackEvent('audio', 'vk-play', songFullName(song));
  }

  var songFullName = function(song){
    return song.artist + ' - ' + song.name;
  }

  var vkMySearch = function(song, callback){
    if (!song.addedStatus){
      var data = {
        audio_ids: [song.vk.aid],
        callback: function(vk){
          if (vk){
            song.addedStatus = 'already_added';          
          }
          callback();
        }
      }
      VkService.getAudio(data);
    }    
  }

  var setAddedStatus = function(scope, song){
    vkMySearch(song, function(){
      if (song.addedStatus != 'already_added'){
        VkService.addAudio(song.vk, function(){
          song.addedStatus = 'just_added';
        });
      }
      scope.$apply()
    });   
  }

  var vkSearch = function(scope, song, position, callback){
    scope.vkProcessing = true;
    var data = {
      q: SongQuery.prepareSearchTerm(song),
      callback: function(vk){
        if (vk){
          song.vk = vk;
          callback(vk);          
        }else{
          var msg = $translate.instant('notification.failed.audio_file_not_found');
          toastr.error(msg);  
          scope.currentSongIndex = position;
          scope.nextPlay();
        }
        scope.vkProcessing = false;
      }
    }
    VkService.searchAudio(data);    
  }

  var paginateSongs = function(scope){
    if (scope.rowCollection.length - scope.currentSongIndex <= 2 && !scope.songScope.stopPagination){
      var tableState = scope.songScope.tableState;
      tableState.pagination.start = tableState.pagination.start + Settings.perPage;
      scope.songScope.getCollection(tableState, {}, function(s){
        scope.rowCollection = s.rowCollection;
        s.tableState = tableState;
      });
    }
  }

  this.build = function(scope){
    angular.extend(scope, vkPlayerExtension);

    scope.volume = ApplicationStore.get('player_volume') || 1;

    scope.minified = ApplicationStore.get('player_minified') == 'true';

    scope.$watch('volume', function(v){
      scope.mediaPlayer.setVolume(v);
      ApplicationStore.set('player_volume', v);
      $rootScope.$broadcast('change_volume', v);
    })

    scope.$on('stop_playing', function(event, type, id){
      if (type != 'vk'){
        scope.mediaPlayer.pause();     
      }
    });

    scope.$watch('mediaPlayer', function(player){
      player.on('ended', function(){
        scope.nextPlay();
      });
    })

    scope.showPlayer = function(){
      return scope.rowCollection && scope.rowCollection.length > 0;
    }

    scope.closePlayer = function(){
      scope.mediaPlayer.stop();
      scope.rowCollection = [];
      scope.songScope = null;
      scope.currentSongIndex = null;
    }

    scope.minifyPlayer = function(){
      scope.minified = !scope.minified;
      ApplicationStore.set('player_minified', scope.minified);
    }

    scope.checkPlaying = function(song){
      return song.id == scope.currentSong.id; 
    }

    scope.checkPlayerToPlaying = function(song){
      return scope.mediaPlayer.playing && scope.checkPlaying(song); 
    }

    scope.addSongToPlaylist = function (audioElement) {
      scope.mediaPlayer.load(audioElement, true);
    };

    scope.nextPlay = function(){
      var song, index = scope.currentSongIndex + 1;
      if (scope.rowCollection && index < scope.rowCollection.length){
        scope.mediaPlayer.stop();
        song = scope.rowCollection[index];
        scope.vkPlayPause(song, index);       
        paginateSongs(scope);
      }
    };

    scope.prevPlay = function(){
      var song, index = scope.currentSongIndex - 1;
      if (scope.rowCollection && index >= 0){
        scope.mediaPlayer.stop();
        song = scope.rowCollection[index];
        scope.vkPlayPause(song, index);       
      }
    };

    scope.$on('vk_login', function(event){
      scope.vkProcessing = false;
    })

    scope.$on('vk_logout', function(event){
      scope.vkProcessing = false;
    })

    scope.play = function(){
      if (this.rowCollection){
        var song = this.rowCollection[scope.currentSongIndex];
        scope.vkPlayPause(song, scope.currentSongIndex);
      }    
    };

    scope.volumeUp = function(){
      var volume = scope.volume + 0.1;
      if (volume <= 1){
        scope.volume = volume;
      }
    }

    scope.volumeDown = function(){
      var volume = scope.volume - 0.1;
      if (volume >= 0){
        scope.volume = volume;
      } 
    }

    scope.vkAddSong = function(song, position){
      if (!song.vk)
      {
        vkSearch(scope, song, position, function(vk){
          setAddedStatus(scope, song);
        });        
      }else{
        setAddedStatus(scope, song);
      }
       Analytics.trackEvent('audio', 'vk-add', songFullName(song));
    }

    scope.scrollToSong = function(song){
      var songElement = angular.element(document.getElementById('song-'+song.id));
      var container = angular.element(document.getElementsByClassName('table-container'));
      if (container.length > 0 && songElement.length > 0){
        container.scrollToElement(songElement, 0, 1000);
      }
    }

    scope.vkPlayPause = function(song, position, songScope){
      if (song) {
        // play from listing
        if (songScope){
          scope.scrollToSong(song);
        }

        if (songScope && scope.songScope != songScope){
          scope.rowCollection = songScope.rowCollection;
          scope.songScope = songScope;
        }

        scope.mediaPlayer.setVolume(scope.volume);

        if (!scope.mediaPlayer.playing){
          $rootScope.$broadcast('stop_playing', 'vk', song.id);
        }

        var prevPosition = scope.currentSongIndex;
        scope.currentSongIndex = position;
        scope.currentSong = song;

        if (position != prevPosition){
          playSong(scope, song, position);
        }else{
          if (song.vk){
            scope.mediaPlayer.playPause();
          }else{
            playSong(scope, song, position);
          }
        }
      }
    }
  }

}]);