'use strict';

angular.module('topPlaylistClientApp').service('ApplicationStore', ['localStorageService', function(localStorageService) {
  this.get = function(key){
    return localStorageService.get(key);
  }

  this.set = function(key, value){
    localStorageService.set(key, value)
  }
}]);