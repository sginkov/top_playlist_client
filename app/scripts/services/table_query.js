'use strict';

angular.module('topPlaylistClientApp').service('TableQuery', ['Settings', function(Settings) {
  this.defaultOptions = function() {
  	return {
    	perPage: Settings.perPage,
      startIndex: 1,
    	query: {},
    	resourceMethod: 'query'
    }
  }

  this.prepareQueryOptions = function(tableState){
    var paginationNumber = tableState.pagination.number || Settings.perPage;
    return {
      per_page: paginationNumber,
      page: Math.ceil(tableState.pagination.start/paginationNumber) + 1,
      sort_by: tableState.sort.predicate,
      sort_order: tableState.sort.reverse ? 'desc' : 'asc',
      q: (tableState.search.predicateObject || {}).q
    }
  }
}]);