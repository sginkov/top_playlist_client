'use strict';

angular.module('topPlaylistClientApp').service('ResourceWrapper', 
  ['$rootScope', '$timeout', 'Source', 'PrimarySource', 'SongRotation', 'PlaylistReport', 'Song', 'UserInfo',
    function($rootScope, $timeout, Source, PrimarySource, SongRotation, PlaylistReport, Song, UserInfo) {
      var resources = {
        Source: Source,
        PrimarySource: PrimarySource,
        SongRotation: SongRotation,
        PlaylistReport: PlaylistReport,
        Song: Song,
        UserInfo: UserInfo
      };

      var defaultSettings = {
        isArray: true,
        timeout: 1000,
        query: {},
        successCallback: function(response){},
        errorCallback: function(response){}
      }

      this.query = function(resourceName, actionName, options){
        var settings = angular.extend({}, defaultSettings, options);
        var resource = resources[resourceName];

        $timeout(function () {
          resource[actionName].call(resource, settings.query).$promise.then(
            //success
            settings.successCallback,
            //error
            settings.errorCallback
          )

        }, settings.timeout);
      }
    }
]);