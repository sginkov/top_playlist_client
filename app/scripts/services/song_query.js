'use strict';

angular.module('topPlaylistClientApp').service('SongQuery', function() {
  this.prepareSearchTerm = function(song){
    return  song.search_query;
  }
});