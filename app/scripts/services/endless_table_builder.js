'use strict';

angular.module('topPlaylistClientApp').service('EndlessTableBuilder', ['ResourceWrapper', 'TableQuery', function(ResourceWrapper, TableQuery) {
  this.build = function(scope, resource, options){
    var tableOptions = angular.extend({}, TableQuery.defaultOptions(), options);
    scope.rowCollection = [];
    scope.isLoading = false;
    angular.extend(scope, tableOptions);

    scope.decorateCollection = function(collection){
      return collection;
    }

    scope.getPosition = function(position, song){
      return position;
    }

    scope.getCollection = function(tableState, tableObject, successHandler){
      scope.isLoading = true;
      scope.rowCollection = scope.rowCollection || [];

      if (!tableState){
        tableState = {
          pagination: {
            number: tableOptions.perPage, 
            start: 0
          },
          search: {},
          sort: {}
        }
      }

      var tableQuery = TableQuery.prepareQueryOptions(tableState);

      angular.extend(tableQuery, tableOptions.query);

      var settings = {
        query: tableQuery,
        successCallback: function(collection){
          var pageCollection = scope.decorateCollection(collection.entities);

          if (pageCollection.length == 0){
            scope.stopPagination = true;
          }

          if (tableState.pagination.start === 0) {
            scope.rowCollection = pageCollection;
          }else{
            scope.rowCollection = scope.rowCollection.concat(pageCollection);                     
          } 

          if (successHandler){
            successHandler(scope);
          }

          scope.$emit('collection_loaded');
          scope.isLoading = false;         
        }
      }

      ResourceWrapper.query(resource, tableOptions.resourceMethod, settings);
    };
  }
}]);