'use strict';

/**
 * @ngdoc overview
 * @name topPlaylistClientApp
 * @description
 * # topPlaylistClientApp
 *
 * Main module of the application.
 */
angular
  .module('topPlaylistClientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'app.config',
    'pascalprecht.translate',
    'checklist-model',
    'angular-carousel',
    'ngCookies',
    'smart-table',
    'angularMoment',
    'ui.router.state',
    'ncy-angular-breadcrumb',
    'ui.select2',
    'LocalStorageModule',
    'mediaPlayer',
    'angular-underscore',
    'duScroll',
    'multi-select',
    'angular-google-analytics',
    'pasvaz.bindonce'
  ])

  .config(['$translateProvider', function ($translateProvider) {
    $translateProvider.preferredLanguage('ru');

    $translateProvider.useStaticFilesLoader({
      prefix: '/languages/',
      suffix: '.json'
    });
  }])

  .config(function($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
      prefixStateName: 'home'
    });
  })

  .config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('HttpInterceptorFactory');
  }])

  .config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('topPlaylistClientApp');
  })

  .constant('Settings', {
    perPage: 20,
    songs:{
      popular:{
        perPage: 10
      }
    }
  })

  .constant('Genres', ['pop', 'dance', 'rock', 'rnd_hiphop', 'indie'])

  .constant('ContactInfo', {
    email: 'top-playlists@yandex.ru',
    companyName: 'TopPlaylist',
    vk: 'https://vk.com/club87047362'
  })

  .value('mp.throttleSettings', {
    enabled: true,
    time: 500
  })

  .config(function(AnalyticsProvider, API) {
    // initial configuration
    AnalyticsProvider.setAccount(API.GA.accountId);
    AnalyticsProvider.useAnalytics(true);
    AnalyticsProvider.setDomainName(API.GA.domain);
    AnalyticsProvider.setPageEvent('$stateChangeSuccess');
  })

  .config(function($locationProvider) {
    $locationProvider.hashPrefix('!');
  })

  .run(function run($translate, ApplicationStore, amMoment, $rootScope, $state, $breadcrumb, API, ResourceWrapper, Analytics, ContactInfo, VkPlayer){
    $rootScope.contactInfo = ContactInfo;

    var useLocale = function(locale){
      $translate.use(locale);
      moment.locale(locale);
      $rootScope.locale = locale;      
    }

    $rootScope.$on('change_local', function(event, locale){
      ApplicationStore.set('lang', locale);
      useLocale(locale)
    })

    useLocale(ApplicationStore.get('lang') || 'ru');

    VK.Observer.subscribe("auth.login", function (event) 
    { 
      $rootScope.session = event.session;
      Analytics.trackEvent('login', 'vk', $rootScope.session.mid);
      $rootScope.session.provider = 'vk';
      $rootScope.$broadcast('vk_login');
    });

    VK.Observer.subscribe("auth.logout", function () 
    { 
      $rootScope.session = null;
      $rootScope.$broadcast('vk_logout');
    });

    $rootScope.$on('vk_login', function(event){
      ResourceWrapper.query('UserInfo', 'get_votes', { 
        query: { provider: $rootScope.session.provider, 
          uid: $rootScope.session.mid, 
        },
        successCallback: function(response){
          $rootScope.session.votes = response.entities;
          $rootScope.$broadcast('get_votes');
        }
      }); 
    });

    VkPlayer.build($rootScope);

    $rootScope.slugId = function(entity){
      if (entity && entity._slugs){
        return entity._slugs[0];
      }
    }

    $rootScope.multiLanguagePartialUrl = function(partial){
      return 'views/partials/multi_languages/'+ $rootScope.locale + '/'+ partial;
    }

    $rootScope.isActive = function(stateName) {
      return $state.includes(stateName);
    }

    // helper function to seek to a percentage
    $rootScope.seekPercentage = function ($event) {
      var offsetX = ($event.offsetX !== undefined) ? $event.offsetX : $event.originalEvent.layerX;
      var percentage = (offsetX / $event.currentTarget.offsetWidth);
      if (percentage <= 1) {
        return percentage;
      } else {
        return 0;
      }
    };

    angular.forEach([0, 403, 404, 408, 500], 
      function(value) {
        $rootScope.$on('error_' + value, function(event, args){
          var msg, errors = args.errors;
          if (errors){
            msg = $translate.instant('error_status_messages.status_' + value, {errors: errors});
          }
          else{
            msg = $translate.instant('error_status_messages.status_' + value);
          }
          toastr.error(msg);  
        });    
      }
    );
  });
