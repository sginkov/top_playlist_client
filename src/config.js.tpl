angular.module('app.config', [])
.constant('API', {
  baseURL: '<%= baseURL %>',
  assetsURL: '<%= assetsURL %>',
  vkSettings: {
    apiId: '<%= vkSettings.apiId %>',
    permissions: <%= vkSettings.permissions %>,
    groupId: <%= vkSettings.groupId %>
  },
  GA:{
  	accountId: '<%= GA.accountId %>',
  	domain: '<%= GA.domain %>'
	}
});